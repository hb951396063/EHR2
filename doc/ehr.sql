/*
 Navicat Premium Data Transfer

 Source Server         : 115
 Source Server Type    : MySQL
 Source Server Version : 50171
 Source Host           : 115.28.2.33
 Source Database       : ehr

 Target Server Type    : MySQL
 Target Server Version : 50171
 File Encoding         : utf-8

 Date: 04/17/2017 14:41:02 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `blood`
-- ----------------------------
DROP TABLE IF EXISTS `blood`;
CREATE TABLE `blood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `blood`
-- ----------------------------
BEGIN;
INSERT INTO `blood` VALUES ('1', 'A型', ''), ('2', 'AB型', 'AB型'), ('3', 'C型', ''), ('5', 'O型', 'O型');
COMMIT;

-- ----------------------------
--  Table structure for `contract`
-- ----------------------------
DROP TABLE IF EXISTS `contract`;
CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `contractname` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `contract`
-- ----------------------------
BEGIN;
INSERT INTO `contract` VALUES ('1', '钟伟,', '', '劳动合同', '劳动合同', '1', '2015-03-29');
COMMIT;

-- ----------------------------
--  Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `department`
-- ----------------------------
BEGIN;
INSERT INTO `department` VALUES ('1', '开发部', ''), ('2', '行政部', ''), ('3', 'DKG部', '');
COMMIT;

-- ----------------------------
--  Table structure for `education`
-- ----------------------------
DROP TABLE IF EXISTS `education`;
CREATE TABLE `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `education`
-- ----------------------------
BEGIN;
INSERT INTO `education` VALUES ('1', '大专2', ''), ('2', '本科', ''), ('3', '硕士', ''), ('4', '博士', '');
COMMIT;

-- ----------------------------
--  Table structure for `educationexperience`
-- ----------------------------
DROP TABLE IF EXISTS `educationexperience`;
CREATE TABLE `educationexperience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) DEFAULT NULL,
  `schoolname` varchar(50) DEFAULT NULL,
  `educationid` varchar(11) DEFAULT NULL,
  `zhuanye` varchar(50) DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `educationexperience`
-- ----------------------------
BEGIN;
INSERT INTO `educationexperience` VALUES ('41', '0', 'asdf', 'asdf', '', null, null, '');
COMMIT;

-- ----------------------------
--  Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(20) DEFAULT NULL,
  `cnname` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `nation` varchar(50) DEFAULT NULL,
  `nativeplace` varchar(50) DEFAULT NULL,
  `hobby` varchar(100) DEFAULT NULL,
  `health` varchar(50) DEFAULT NULL,
  `native1` varchar(200) DEFAULT NULL,
  `icno` varchar(30) DEFAULT NULL,
  `issue` varchar(100) DEFAULT NULL,
  `idaddress` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `linkman` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `emailwork` varchar(50) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `school` varchar(50) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `genderid` int(11) DEFAULT NULL,
  `partyid` int(11) DEFAULT NULL,
  `bloodid` int(11) DEFAULT NULL,
  `marital` int(11) DEFAULT NULL,
  `procreateid` int(11) DEFAULT NULL,
  `nativetypeid` int(11) DEFAULT NULL,
  `ictypeid` int(11) DEFAULT NULL,
  `educationid` int(11) DEFAULT NULL,
  `usefullife` date DEFAULT NULL,
  `graduatetime` date DEFAULT NULL,
  `startworkingdate` date DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `employee`
-- ----------------------------
BEGIN;
INSERT INTO `employee` VALUES ('1', 'AE1001,', '钟伟,', 'zhongwei,', ',', ',', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, '2015-03-22', '2015-03-22', '2015-03-22', '2015-03-22'), ('2', 'AE1001,AE1001,', '钟伟,钟伟,', 'zhongwei,zhongwei,', ',,', ',,', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, '2015-03-22', '2015-03-22', '2015-03-22', '2015-03-22'), ('3', 'AE1003', '钟伟2', 'zhongwei', '汉', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, '30', '1', '1', '1', '1', '1', '1', '1', '2', '2015-03-23', '2015-03-23', '2015-03-23', '2015-03-23'), ('4', 'AE1001,', '钟伟,', 'zhongwei,', ',', ',', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '1', '1', '1', '1', '1', '1', '1', '1', '2015-03-22', '2015-03-22', '2015-03-22', '2015-03-22'), ('5', 'AE1001,', '钟伟,', 'zhongwei,', ',', ',', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '1', '1', '1', '1', '1', '1', '1', '1', '2015-03-22', '2015-03-22', '2015-03-22', '2015-03-22'), ('6', 'AE1001,AE1001,', '钟伟,钟伟,', 'zhongwei,zhongwei,', ',,', ',,', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '1', '1', '1', '1', '1', '1', '1', '1', '2015-03-22', '2015-03-22', '2015-03-22', '2015-03-22'), ('7', 'AE1002', null, '秦奉', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null), ('8', 'AE1001,', '钟伟', 'zhongwei,', ',', ',', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '1', '1', '1', '1', '1', '1', '1', '1', '2015-03-22', '2015-03-22', '2015-03-22', '2015-03-22'), ('9', 'AE1002', '秦奉', 'QinFeng', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '2', '3', '5', '1', '1', '1', '1', '2', null, null, null, null), ('10', 'AE1033', null, '周张琳', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null), ('11', 'AE1033', '周张琳', '周张琳', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '1', '1', '1', '1', '1', '1', '1', '1', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `entry`
-- ----------------------------
DROP TABLE IF EXISTS `entry`;
CREATE TABLE `entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(30) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `team` varchar(50) DEFAULT NULL,
  `transfer` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `entry`
-- ----------------------------
BEGIN;
INSERT INTO `entry` VALUES ('1', 'AE1002', '秦奉', 'NDP', '产品经理', '1', null, '2015-03-29'), ('2', 'AE1033', '周张琳', 'NPG', '', '', null, '2015-04-03');
COMMIT;

-- ----------------------------
--  Table structure for `entrystate`
-- ----------------------------
DROP TABLE IF EXISTS `entrystate`;
CREATE TABLE `entrystate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `exitinfo`
-- ----------------------------
DROP TABLE IF EXISTS `exitinfo`;
CREATE TABLE `exitinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exitform` varchar(50) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `reason2` varchar(200) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `lastday` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `exitinfo`
-- ----------------------------
BEGIN;
INSERT INTO `exitinfo` VALUES ('1', '合同到期', '合同到期', '合同到期', '合同到期', '钟伟', '1', '2015-03-27');
COMMIT;

-- ----------------------------
--  Table structure for `familymembers`
-- ----------------------------
DROP TABLE IF EXISTS `familymembers`;
CREATE TABLE `familymembers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `politicalstatus` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `gender`
-- ----------------------------
DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `gender`
-- ----------------------------
BEGIN;
INSERT INTO `gender` VALUES ('1', 'Male-男', '男'), ('2', 'Female-女', '女'), ('3', '其他', '');
COMMIT;

-- ----------------------------
--  Table structure for `ictype`
-- ----------------------------
DROP TABLE IF EXISTS `ictype`;
CREATE TABLE `ictype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `ictype`
-- ----------------------------
BEGIN;
INSERT INTO `ictype` VALUES ('1', '身份证', ''), ('2', '驾驶证', '驾驶证');
COMMIT;

-- ----------------------------
--  Table structure for `info`
-- ----------------------------
DROP TABLE IF EXISTS `info`;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `corpname` varchar(50) DEFAULT NULL,
  `name2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `info`
-- ----------------------------
BEGIN;
INSERT INTO `info` VALUES ('1', '四川摩奇HR管理系统 v1.0', '四川摩奇', 'MK EHR1.0');
COMMIT;

-- ----------------------------
--  Table structure for `jinengpj`
-- ----------------------------
DROP TABLE IF EXISTS `jinengpj`;
CREATE TABLE `jinengpj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `pingjia` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `jinengpj`
-- ----------------------------
BEGIN;
INSERT INTO `jinengpj` VALUES ('41', '0', 'Java', '精通');
COMMIT;

-- ----------------------------
--  Table structure for `job`
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `job`
-- ----------------------------
BEGIN;
INSERT INTO `job` VALUES ('1', '移动前端开发', '');
COMMIT;

-- ----------------------------
--  Table structure for `level`
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `level`
-- ----------------------------
BEGIN;
INSERT INTO `level` VALUES ('1', '初级', ''), ('2', '中级', ''), ('3', '高级', '');
COMMIT;

-- ----------------------------
--  Table structure for `marital`
-- ----------------------------
DROP TABLE IF EXISTS `marital`;
CREATE TABLE `marital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `marital`
-- ----------------------------
BEGIN;
INSERT INTO `marital` VALUES ('1', '未婚', ''), ('2', '已婚', '');
COMMIT;

-- ----------------------------
--  Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cnname` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `ordernum` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `menu`
-- ----------------------------
BEGIN;
INSERT INTO `menu` VALUES ('8', '0', 'info', '基础数据', '＃', '1', '0'), ('9', '0', 'sys', '系统管理', '#', '1', '60'), ('10', 'sys', 'user', '用户管理', '/user', '1', '0'), ('11', 'sys', 'menu', '菜单管理', '/menu', '1', '0'), ('12', 'sys', 'info', '信息设置', '/info', '1', '0'), ('13', 'info', 'gender', '性别', '/gender', '1', '0'), ('14', 'info', 'worktype', '工作类型', '/worktype', '1', '0'), ('15', 'info', 'job', '职位', '/job', '1', '0'), ('16', 'info', 'level', '职级', '/level', '1', '0'), ('17', 'info', 'productline', '产品线', '/productline', '1', '0'), ('18', 'info', 'productteam', '产品组', '/productteam', '1', '0'), ('19', 'info', 'entrystate', '入职状态', '/entrystate', '1', '0'), ('20', 'info', 'education', '学历', '/education', '1', '0'), ('21', 'info', 'marital', '婚姻', '/marital', '1', '0'), ('22', 'info', 'blood', '血型', '/blood', '1', '0'), ('23', 'info', 'procreate', '生育状态', '/procreate', '1', '0'), ('24', '0', 'em', '员工管理', '#', '1', '10'), ('25', 'em', 'employee', '员工信息', '/employee', '1', '2'), ('26', 'em', 'department', '部门管理', '/department', '1', '1'), ('27', 'info', 'party', '政治面貌', '/party', '1', '0'), ('28', 'info', 'nativetype', '户籍类型', '/nativetype', '1', '0'), ('29', 'info', 'ictype', '证件类型', '/ictype', '1', '0'), ('30', 'em', 'transfer', '人事调动', '/employee', '1', '9'), ('31', 'em', 'exitinfo', '离职查询', '/exitinfo', '1', '10'), ('32', 'em', 'train', '培训管理', '/department', '1', '8'), ('33', '0', 'at', '考勤管理', '#', '1', '20'), ('34', 'at', 'vocation', '请假管理', '/vocation', '1', null), ('35', 'at', 'overtime', '加班管理', '/overtime', '1', null), ('36', 'em', 'positiontransfer', '岗位调动', '/positiontransfer', '1', '9'), ('37', 'em', 'entry', '入职管理', '/entry', '1', '3'), ('38', 'em', 'raise', '加薪管理', '/raise', '1', '7'), ('39', 'em', 'rewards', '奖惩管理', '/rewards', '1', '6'), ('40', 'em', 'positive', '转正管理', '/positive', '1', '4'), ('41', 'em', 'contract', '合同管理', '/contract', '1', '5');
COMMIT;

-- ----------------------------
--  Table structure for `nativetype`
-- ----------------------------
DROP TABLE IF EXISTS `nativetype`;
CREATE TABLE `nativetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `nativetype`
-- ----------------------------
BEGIN;
INSERT INTO `nativetype` VALUES ('1', '城市居民', '城市居民'), ('2', '弄明', '');
COMMIT;

-- ----------------------------
--  Table structure for `party`
-- ----------------------------
DROP TABLE IF EXISTS `party`;
CREATE TABLE `party` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `party`
-- ----------------------------
BEGIN;
INSERT INTO `party` VALUES ('1', '党员', ''), ('2', '团员', ''), ('3', '群众', '');
COMMIT;

-- ----------------------------
--  Table structure for `positiontransfer`
-- ----------------------------
DROP TABLE IF EXISTS `positiontransfer`;
CREATE TABLE `positiontransfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `team1` varchar(50) DEFAULT NULL,
  `team2` varchar(50) DEFAULT NULL,
  `transfer1` varchar(50) DEFAULT NULL,
  `transfer2` varchar(50) DEFAULT NULL,
  `reason` varchar(200) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `positiontransfer`
-- ----------------------------
BEGIN;
INSERT INTO `positiontransfer` VALUES ('1', '钟伟,', 'K1', 'K2', 'ios', 'game', '公司业务调整', '', '1', '2015-03-27');
COMMIT;

-- ----------------------------
--  Table structure for `positive`
-- ----------------------------
DROP TABLE IF EXISTS `positive`;
CREATE TABLE `positive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `positive`
-- ----------------------------
BEGIN;
INSERT INTO `positive` VALUES ('1', '秦奉', '', '', '9', '2015-03-29');
COMMIT;

-- ----------------------------
--  Table structure for `procreate`
-- ----------------------------
DROP TABLE IF EXISTS `procreate`;
CREATE TABLE `procreate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `procreate`
-- ----------------------------
BEGIN;
INSERT INTO `procreate` VALUES ('1', '未生育', ''), ('2', '已生育', ''), ('3', 'asdf', '');
COMMIT;

-- ----------------------------
--  Table structure for `productline`
-- ----------------------------
DROP TABLE IF EXISTS `productline`;
CREATE TABLE `productline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `productline`
-- ----------------------------
BEGIN;
INSERT INTO `productline` VALUES ('1', 'KIDS', ''), ('2', 'HIDD', '');
COMMIT;

-- ----------------------------
--  Table structure for `productteam`
-- ----------------------------
DROP TABLE IF EXISTS `productteam`;
CREATE TABLE `productteam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `raise`
-- ----------------------------
DROP TABLE IF EXISTS `raise`;
CREATE TABLE `raise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `rewards`
-- ----------------------------
DROP TABLE IF EXISTS `rewards`;
CREATE TABLE `rewards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `money` varchar(100) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `rewards`
-- ----------------------------
BEGIN;
INSERT INTO `rewards` VALUES ('1', '秦奉', '罚款100', '迟到', '', '9', '1', '2015-03-29');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'dongfangx', '50f7aafcb35f1610ef608419e9c93aef', 'dongfang.x@qq.com', '18982280521', '0'), ('2', 'zw', '6a204bd89f3c8348afd5c77c717a097a', 'dongfang.x@qq.com', '13882295401', null);
COMMIT;

-- ----------------------------
--  Table structure for `workexperience`
-- ----------------------------
DROP TABLE IF EXISTS `workexperience`;
CREATE TABLE `workexperience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) DEFAULT NULL,
  `post` varchar(50) DEFAULT NULL,
  `corpname` varchar(50) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `workexperience`
-- ----------------------------
BEGIN;
INSERT INTO `workexperience` VALUES ('41', '1', 't1', 't1', null, null, '', ''), ('42', '2', '52', '52', null, null, '', '');
COMMIT;

-- ----------------------------
--  Table structure for `worktype`
-- ----------------------------
DROP TABLE IF EXISTS `worktype`;
CREATE TABLE `worktype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `worktype`
-- ----------------------------
BEGIN;
INSERT INTO `worktype` VALUES ('1', '全职', ''), ('2', '实习', ''), ('3', '兼职', '');
COMMIT;

-- ----------------------------
--  Table structure for `xmjinyan`
-- ----------------------------
DROP TABLE IF EXISTS `xmjinyan`;
CREATE TABLE `xmjinyan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `zhizhe` varchar(50) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `xmjinyan`
-- ----------------------------
BEGIN;
INSERT INTO `xmjinyan` VALUES ('41', '1', 'ee', 'ee', null, null, '', '', ''), ('42', '2', 'aa', 'aa', null, null, '', '', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
